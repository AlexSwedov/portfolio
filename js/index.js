
// основни свайпер сторінки
// new Swiper('.sections-slider', {

// 	direction: 'vertical',

// 	scrollbar: {
// 		el: '.sections-slider__scrollbar',
// 		draggable: true,
// 	},

// 	keyboard: {
// 		enabled: true,
// 		onlyInViewport: true,
// 		pageUpDow: true,
// 	},

// 	mousewheel: {
// 		sensetivy: 1,
// 		eventsTarget: '.sections-slider',
// 	},

// 	parallax: true,
// 	speed: 1000,
// });

//---------------------------------------------

document.addEventListener('DOMContentLoaded', (event) => {
	const swiper = new Swiper('.sections-slider', {
		direction: 'vertical',
		scrollbar: {
			el: '.sections-slider__scrollbar',
			draggable: true,
		},
		keyboard: {
			enabled: true,
			onlyInViewport: true,
			pageUpDown: true,
		},
		mousewheel: {
			sensitivity: 1,
			eventsTarget: '.sections-slider',
		},
		parallax: true,
		speed: 1000,
		on: {
			wheel: function (event) {
				const scrollableContent = document.querySelector('#scroll');
				const isInsideScrollableContent =
					event.target === scrollableContent || scrollableContent.contains(event.target);

				if (isInsideScrollableContent) {
					event.preventDefault();
					scrollableContent.scrollTop += event.deltaY;
				}
			},
		},
	});

	const scrollableContent = document.querySelector('#scroll');
	let isScrolling = false; // Флаг для відстеження стану прокрутки
	let startY;
	let scrollTop;

	// Відключаємо обробку події прокрутки колесом миші в Swiper.js
	// під час перебування курсору в прокручуваному блоці
	scrollableContent.addEventListener('mouseenter', function () {
		swiper.mousewheel.disable();
	});

	// Вмикаємо обробку події прокрутки колесом миші в Swiper.js
	// після виходу курсору з прокручуваного блоку
	scrollableContent.addEventListener('mouseleave', function () {
		swiper.mousewheel.enable();
	});

	// Запобігаємо скролу сторінки та забезпечуємо плавну скролінг вмісту блоку
	scrollableContent.addEventListener('wheel', function (event) {
		const isScrolledToBottom =
			this.scrollHeight - this.scrollTop === this.clientHeight;
		const isScrolledToTop = this.scrollTop === 0;

		if (isScrolledToBottom && event.deltaY > 0) {
			swiper.mousewheel.enable();
		} else if (isScrolledToTop && event.deltaY < 0) {
			swiper.mousewheel.enable();
		} else {
			swiper.mousewheel.disable();
			event.preventDefault();

			if (!isScrolling) {
				isScrolling = true;
				window.requestAnimationFrame(smoothScroll.bind(this, event.deltaY));
			}
		}
	});

	// Функція плавної прокрутки
	function smoothScroll(delta) {
		this.scrollBy(0, delta);

		if (Math.abs(delta) > 10) {
			delta = delta > 0 ? delta - 10 : delta + 10;
			window.requestAnimationFrame(smoothScroll.bind(this, delta));
		} else {
			isScrolling = false;
		}
	}

	// Обробка події перетягування миші
	scrollableContent.addEventListener('mousedown', dragStart);
	scrollableContent.addEventListener('touchstart', dragStart);

	// Функція запуску перетягування
	function dragStart(e) {
		isScrolling = true;
		startY = e.pageY || e.touches[0].pageY;
		scrollTop = scrollableContent.scrollTop;
	}

	// Обробка події переміщення миші або пальця під час перетягування
	scrollableContent.addEventListener('mousemove', dragging);
	scrollableContent.addEventListener('touchmove', dragging);

	// Функція перетягування
	function dragging(e) {
		if (!isScrolling) return;
		e.preventDefault();
		let y = e.pageY || e.touches[0].pageY;
		let walk = (y - startY) * 1.5; // Коефіцієнт швидкості прокрутки
		scrollableContent.scrollTop = scrollTop - walk;
	}

	// Обробка події завершення перетягування
	scrollableContent.addEventListener('mouseup', dragStop);
	scrollableContent.addEventListener('touchend', dragStop);

	// Функція завершення перетягування
	function dragStop() {
		isScrolling = false;
	}
});